import numpy as np

def rand_gaussian_2D(Lbox, Nsize, powerspectrum, seed=40, Helmholtz_decomp=False, fSol=None, norm=False, meanfield=None):
    if (Helmholtz_decomp) & (fSol==None):
        raise ValueError("fSol must be specified if Helmholtz_decomp is True")
    if (norm) & (meanfield==None):
        raise ValueError("meanfield must be specified if norm is True")

    Lcell = Lbox/float(Nsize)
    kSpace = 2*np.pi*np.fft.fftfreq(Nsize, Lcell)

    # generate mesh of [kx,ky]
    kx, ky = np.meshgrid(kSpace,kSpace)

    # calculate absolute k
    kk = kx*kx + ky*ky
    k = np.sqrt(kk)

    # assign power giving k: power can be inf at k=0
    ps = powerspectrum(k)
    ps[ps == np.inf] = 0.
    # the std of gaussian variate is the sqrt of power
    gStd = np.sqrt(ps)

    # Gaussian random field in k-space:

    # x-axis: Vx = Ax + iBx
    np.random.seed(seed)
    Ax = np.random.normal(scale=gStd)
    Bx = np.random.normal(scale=gStd)
    # y-axis: Vy = Ay + iBy
    Ay = np.random.normal(scale=gStd)
    By = np.random.normal(scale=gStd)

    # Helmholtz decomposition
    if Helmholtz_decomp:
        fComp = 1.0 - fSol
        ## remove k=0 case
        kk = np.clip(kk, kk[0,1],np.max(kk))
        ka = kx*Ax + ky*Ay
        kb = kx*Bx + ky*By

        ## decompose for Vx
        diva = kx*ka/kk
        divb = kx*kb/kk
        curla = Bx - divb
        curlb = Ax - diva
        Ax = fSol*curla + fComp*divb
        Bx = fSol*curlb + fComp*diva

        ## decompose for Vy
        diva = ky*ka/kk
        divb = ky*kb/kk
        curla = By - divb
        curlb = Ay - diva
        Ay = fSol*curla + fComp*divb
        By = fSol*curlb + fComp*diva

    vxKspace = np.vectorize(complex)(Ax, Bx)
    vyKspace = np.vectorize(complex)(Ay, By)

    vxReal = np.fft.fftn(vxKspace).real
    vyReal = np.fft.fftn(vyKspace).real

    if norm:
        factor = meanfield/np.sqrt(np.sum(vxReal*vxReal + vyReal*vyReal)/float(Nsize*Nsize))
        vxReal *= factor
        vyReal *= factor

    return vxReal, vyReal

def rand_gaussian_3D(Lbox, Nsize, powerspectrum, seed=40, Helmholtz_decomp=False, fSol=None, norm=False, meanfield=None):
    if (Helmholtz_decomp) & (fSol==None):
        raise ValueError("fSol must be specified if Helmholtz_decomp is True")
    if (norm) & (meanfield==None):
        raise ValueError("meanfield must be specified if norm is True")

    Lcell = Lbox/float(Nsize)
    kSpace = 2*np.pi*np.fft.fftfreq(Nsize, Lcell)

    # generate mesh of [kx,ky,kz]
    kx, ky, kz = np.meshgrid(kSpace,kSpace,kSpace,indexing='ij')

    # calculate absolute k
    kk = kx*kx + ky*ky + kz*kz
    k = np.sqrt(kk)

    # assign power giving k: power can be inf at k=0
    ps = powerspectrum(k)
    ps[ps == np.inf] = 0.
    # the std of gaussian variate is the sqrt of power
    gStd = np.sqrt(ps)

    # Gaussian random field in k-space:

    # x-axis: Vx = Ax + iBx
    np.random.seed(seed)
    Ax = np.random.normal(scale=gStd)
    Bx = np.random.normal(scale=gStd)
    # y-axis: Vy = Ay + iBy
    Ay = np.random.normal(scale=gStd)
    By = np.random.normal(scale=gStd)
    # z-axis: Vz = Az + iBz
    Az = np.random.normal(scale=gStd)
    Bz = np.random.normal(scale=gStd)

    # Helmholtz decomposition
    if Helmholtz_decomp:
        fComp = 1.0 - fSol
        ## remove k=0 case
        kk = np.clip(kk, kk[0,1],np.max(kk))
        ka = kx*Ax + ky*Ay + kz*Az
        kb = kx*Bx + ky*By + kz*Bz

        ## decompose for Vx
        diva = kx*ka/kk
        divb = kx*kb/kk
        curla = Bx - divb
        curlb = Ax - diva
        Ax = fSol*curla + fComp*divb
        Bx = fSol*curlb + fComp*diva

        ## decompose for Vy
        diva = ky*ka/kk
        divb = ky*kb/kk
        curla = By - divb
        curlb = Ay - diva
        Ay = fSol*curla + fComp*divb
        By = fSol*curlb + fComp*diva

        ## decompose for Vz
        diva = kz*ka/kk
        divb = kz*kb/kk
        curla = Bz - divb
        curlb = Az - diva
        Az = fSol*curla + fComp*divb
        Bz = fSol*curlb + fComp*diva

    vxKspace = np.vectorize(complex)(Ax, Bx)
    vyKspace = np.vectorize(complex)(Ay, By)
    vzKspace = np.vectorize(complex)(Az, Bz)

    vxReal = np.fft.fftn(vxKspace).real
    vyReal = np.fft.fftn(vyKspace).real
    vzReal = np.fft.fftn(vzKspace).real

    if norm:
        factor = meanfield / np.sqrt( np.sum(vxReal*vxReal+vyReal*vyReal+vzReal*vzReal) / float(Nsize*Nsize*Nsize) )
        vxReal *= factor
        vyReal *= factor
        vzReal *= factor

    return vxReal, vyReal, vzReal

import scipy.interpolate as interp

def map_to_particle_2D(xgrid, ygrid, vxGrid, vyGrid, partCoord):
    vxPart = interp.interpn((xgrid, ygrid), vxGrid, partCoord)
    vyPart = interp.interpn((xgrid, ygrid), vyGrid, partCoord)
    return vxPart, vyPart

def map_to_particle_3D(xgrid, ygrid, zgrid, vxGrid, vyGrid, vzGrid, partCoord):
    vxPart = interp.interpn((xgrid, ygrid, zgrid), vxGrid, partCoord)
    vyPart = interp.interpn((xgrid, ygrid, zgrid), vyGrid, partCoord)
    vzPart = interp.interpn((xgrid, ygrid, zgrid), vzGrid, partCoord)
    return vxPart, vyPart, vzPart
