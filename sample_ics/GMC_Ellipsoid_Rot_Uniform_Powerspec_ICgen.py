import simread.readsnapHDF5 as ws
import numpy as np
import sys
sys.path.insert(0, '/n/home02/hliastro/code/rand_gaussian/src/')
from gaussian_random_field import rand_gaussian_3D, map_to_particle_3D

# physical constants
kb = 1.38e-16
mp = 1.67e-24
mu = 0.67

# GMC parameters
GAMMA = 5/3.
mGMC = 1e4 # Msun
T = 10 # K
aEllip = 3 # pc
bEllip = 2 # pc
cEllip = 1 # pc
#omegaRot = 0.3 # Myr^-1, need to be calculated beforehand based on the fraction of potential energy in rotational energy
omegaRot = 0.0 # Myr^-1, need to be calculated beforehand based on the fraction of potential energy in rotational energy
NLinear = 128 # resolution in 1D
icname = './IC_GMC_Turb_k2'+str(NLinear)+'.hdf5'
fSol = 1.0
vTurb = 4.0
def powerspectrum(k):
    return 1./k**2

# derived quantities
NBox = NLinear*NLinear*NLinear
volEllip = 4*np.pi/3.*aEllip*bEllip*cEllip
volBox = 8*aEllip*bEllip*cEllip
cs = np.sqrt(GAMMA*kb*T/mp)/1e5 # in km/s
print "cs=%f km/s"%cs
utherm = cs*cs/GAMMA/(GAMMA-1.0) # in (km/s)^2

# position in 3D rectangular
#grid = np.linspace(-1*aEllip, aEllip, NLinear+1)
grid = np.linspace(0.0, 2*aEllip, NLinear+1)
x, y, z = np.meshgrid(grid, grid, grid)
x = x.flatten()
y = y.flatten()
z = z.flatten()

# select ellipsoidal region
iEllip = (x/aEllip)*(x/aEllip) + (y/bEllip)*(y/bEllip) + (z/cEllip)*(z/cEllip) <= 1.0
NTot = np.sum(iEllip)

# particle id
print "About to set ids..."
id = np.arange(1,NTot+1,1)
print "Ids are set from %d to %d"%(np.min(id),np.max(id))

print "Setting particle positions"
xPart = x[iEllip]
yPart = y[iEllip]
zPart = z[iEllip]

print "Setting particle velocity"
vxPart = -1*omegaRot*yPart
vyPart = omegaRot*xPart
vzPart = np.repeat(0.0, NTot)

print "Average rotation velocity:", np.sqrt( np.sum( vxPart*vxPart + vyPart*vyPart + vzPart*vzPart ) / NTot )

print "Setting turbulence velocity"
dvxGrid, dvyGrid, dvzGrid = rand_gaussian_3D(aEllip, NLinear+1, powerspectrum, seed=40, Helmholtz_decomp=False, fSol=fSol, norm=True, meanfield=vTurb)
print "Field strength before interpolation:", np.sqrt( np.sum(dvxGrid*dvxGrid+dvyGrid*dvyGrid+dvzGrid*dvzGrid)/float(np.size(dvxGrid)) )
dvx, dvy, dvz = map_to_particle_3D(grid, grid, grid, dvxGrid, dvyGrid, dvzGrid, np.column_stack((xPart, yPart, zPart)))
vxPart += dvx
vyPart += dvy
vzPart += dvz

print "Average kinetic velocity after adding turbulence:", np.sqrt( np.sum( vxPart*vxPart + vyPart*vyPart + vzPart*vzPart ) / NTot )

print "Setting mass and internal energy"
uPart = np.repeat(utherm, NTot)
mPart = np.repeat(mGMC/float(NTot), NTot)
print "gas particle mass=%f"%(mGMC/float(NTot))

print "Writing snapshot"
f=ws.openfile(icname)
ws.write_block(f, "POS ", 0, np.array([xPart,yPart,zPart]).T)
ws.write_block(f, "VEL ", 0, np.array([vxPart,vyPart,vzPart]).T)
ws.write_block(f, "U   ", 0, uPart)
ws.write_block(f, "MASS", 0, mPart)
ws.write_block(f, "ID  ", 0, id)

npart=[NTot,0,0,0,0,0]
massarr=[0,0,0,0,0,0]

header=ws.snapshot_header(npart=npart, nall=npart, massarr=massarr)

ws.writeheader(f, header)
ws.closefile(f)

print "\n-FINISHED-\n"
